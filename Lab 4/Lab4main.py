'''
@file Lab4main.py
@details    Serves as a main file to collect temperature data from the MCP9808 
            temperature sensor every minute or so. It first clears the csv 
            'TempData' (where recorded temperature data and timestamps are 
            stored), and then writes a new data point everytime the data is 
            recorded. Will run continuously until the User presses Ctrl+C.
@author Jacob Burghgraef
@date May 13th 2021

Documentation for this class can be found here:
    https://bitbucket.org/jburghgraef/me405_labs/src/master/Lab%204/Lab4main.py
'''
import utime 
from mcp9808 import mcp9808
from pyb import I2C, Pin
from array import array

## The I2C object representing the Nucleo
i2c = I2C(1, I2C.MASTER)
    
## The address representing the MCP9808 Temperature Sensor
address = 0x18
    
## The object representing the MCP9808 Temperature Sensor
MCP9808 = mcp9808(i2c, address)

## The current timestamp
curr_time = 0

## The pin representing the LED
PinA5 = Pin(Pin.cpu.A5, Pin.OUT_PP)

## The array of temperature data
temp = array('f')

## The array of timestamp data
times = array('f')

if (MCP9808.check() == False):
    print('Invalid sensor detected.')
    print('Please ensure that the MCP9808 temperature sensor is connected.')
else:
    # First, clear the data in the csv file
    with open ("TempData.csv", "r+") as csvfile:
        while True:
            print('Clearing CSV')
            if (csvfile.readline() == ''):
                # Check if the csv line is empty
                print('CSV Cleared')
                break;
            else:
                print('Clearing CSV line')
                csvfile.write('')
            
    # Wait for the button to be pressed before collecting data
    try:
        # Begin data collection
        
        print("Data Collection started")
            
        PinA5.high() # Turn on the LED to indicate data is being collected.
        
        ## The timestamp at which data collection begins.
        start_time = utime.ticks_ms()
        
        while True:
            curr_time = utime.ticks_ms() # Get current timestamp
            temp.append(MCP9808.celsius()) # Record Temperature data
            print("Temperature = ", str(temp[-1]), " deg C")
            times.append(utime.ticks_diff(curr_time,start_time)/1000) # Record time data
            print("at Time = ", str(times[-1]), " seconds")
            with open ("TempData.csv", "a") as csvfile:
                # Append the data collected to the csv file
                csvfile.write("{t}, {T}\r\n".format (t=times[-1],T=temp[-1]))
            utime.sleep(60) # Wait 60 seconds
            
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Terminating Data Collection.')
        PinA5.low() # Turn off the LED to indicate that data collection has stopped
        