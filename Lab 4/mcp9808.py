'''
@file mcp9808.py
@brief A driver for interacting with a MCP9808 temperature sensor using I2C
@author Jacob Burghgraef
@date May 13th 2021

Documentation for this class can be found here:
    https://bitbucket.org/jburghgraef/me405_labs/src/master/Lab%204/mcp9808.py
'''

from pyb import I2C
import utime

class mcp9808:
    '''
    An object representing the MCP9808 Temperature Sensor, it has 3 functions:
        1. check(), which validates that the connected sensor is the MCP9808
        2. celsius(), which calculates the recorded temperature in degrees Celsius
        3. fahrenheit(), which calculates the recorded temperature in degrees Fahrenheit
    '''
    def __init__(self, I2C, address):
        
        ## The I2C object that interfaces with the sensor
        self.I2C = I2C
        
        ## The address to the register that contains the data of interest
        self.address = address
        
        ## An array to hold the data read from the address
        self.data = bytearray(2)
        
    def check(self):
        '''
        Verifies that the sensor is attached at the given bus address by 
        checking that the value in the manufacturer ID register is correct.
        @return     Returns false if the Most Significant Byte (MSB) and Least 
                    Significant Byte (LSB) are incorrect
        '''
        # Read 2 bytes from the slave (at address 0x18); read from register 6
        self.data = self.I2C.mem_read(2, self.address, 0x06) 
        
        if ((self.data[0]<<8|self.data[1]) == 0x0054): 
            # Check if MSB and LSB are correct using a bitshift
            return True
        else:
            return False
        
    def celsius(self):
        '''
        Returns the measured temperature in degrees Celsius.
        '''
        # Get the data read on the MCP9808
        self.data = self.I2C.mem_read(2, self.address, 0x05)
        ## The Most Significant Bit (MSB)
        MSB = self.data[0]
        ## The Least Significant Bit (LSB)
        LSB = self.data[1]
        # Calculate the temperature in deg C
        MSB = MSB & 0x1F # Clear Flag bits
        if ((MSB & 0x10) == 0x10):
            # If the temperature is less than zero
            MSB = MSB & 0x0F
            T_c = 256 - (MSB*16 + LSB/16)
        else:
            # Temperature is greater than zero
            T_c = (MSB*16 + LSB/16)
        return T_c
        
    def fahrenheit(self):
        '''
        Returns the measured temperature in degrees Fahrenheit.
        '''
        # The following process is functionally equivalent to Celsius()
        
        # Get the data read on the MCP9808
        self.data = self.I2C.mem_read(2, self.address, 0x05)
        # The Most Significant Bit (MSB)
        MSB = self.data[0]
        # The Least Significant Bit (LSB)
        LSB = self.data[1]
        # Calculate the temperature in deg C
        MSB = MSB & 0x1F # Clear Flag bits
        if ((MSB & 0x10) == 0x10):
            # If the temperature is less than zero
            MSB = MSB & 0x0F
            T_c = 256 - (MSB*16 + LSB/16)
        else:
            # Temperature is greater than zero
            T_c = (MSB*16 + LSB/16)
            
        # Convert the temperature to Fahrenheit
        T_f = 32 + 9/5*T_c
        return T_f
        
if __name__ == "__main__":
    # Test the from the driver
    
    ## The current timestamp
    current_time = 0
    
    ## The I2C object representing the Nucleo
    i2c = I2C(1, I2C.MASTER)
    
    ## The address representing the MCP9808 Temperature Sensor
    address = 0x18
    
    ## The object representing the MCP9808 Temperature Sensor
    MCP9808 = mcp9808(i2c, address)
    
    # Check that this is indeed a MCP9808 temp sensor
    if (MCP9808.check()):
        start_time = utime.ticks_us() # Start taking data
        while True:
            # Take data 1 time every second
            utime.sleep_us(10**6)
            current_time = utime.ticks_us()
            time = utime.ticks_diff(current_time, start_time)/10**6
            print("Temperature: ", str(MCP9808.celsius()), " degrees C at time ", str(time), " seconds.")
    else:
        # Incorrect device
        print('Invalid device detected, please see that you have a MCP9808 Temperature Sensor Connected.')