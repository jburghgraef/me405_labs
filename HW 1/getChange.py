'''
@file getChange.py

@author Jacob Burghgraef

@date April 6th, 2021

@brief This file is calculates change for a given price and payment.

@details This file takes a given integer price in US cents and payment in the 
         form of an array representing the various denominations of common US
         currency, calculates the necessary change, and returns the change in 
         the same form as the payment. This is intended for use in a vending 
         machine.
'''

# Import math package to use the round down function when calculating change
import math

def getChange(price, payment):
    '''
    @brief  Calculates the change from a given price and payment.
    @param price    The integer price of some given product in US cents.
    @param payment  The amount paid broken into an 8 element array representing
                    the various denominations of US currency, from left to right
                    they are pennies, nickels, dimes, quarters, 1 dollar bills,
                    5 dollar bills, 10 dollar bills, and 20 dollar bills. Note
                    that each element in the array represents the number of 
                    coins or bills to be dispensed as change.
    @return chng    The amount of change returned; in the same format as the 
                    payment.

    '''
    
    # Check for integer parameters
    try:
    
    # Check if the price input is valid
        if int(price) < 0:
            invalid = "Invalid input, please put a positive integer for the price"
            print(invalid)
            return False
    
    # Check is the payment input is valid
        for n in range(len(payment)):
            if int(payment[n]) < 0:
                invalid = "Invalid input, please put zero or a positive integer for each denomination of currency in the payment"
                print(invalid)
                return False  
    
    # Catch improper data types in the parameter
    except ValueError:
        print("Invalid parameter, please input a positive integer for price.")
        print("For payment, input a positive integer or zero for each denomination of currency.")
        return False
    
    ## A list indexing the value of each acceptable denomination of US currency, all in cents
    Denominations = [
        1,              # Represents the value of a penny in cents
        5,              # Represents the value of a nickel in cents
        10,             # Represents the value of a dime in cents
        25,             # Represents the value of a quarter in cents
        100,            # Represents the value of a 1 dollar in cents
        500,            # Represents the value of a 5 dollars in cents
        1000,           # Represents the value of a 10 dollars in cents
        2000]           # Represents the value of a 20 dollars in cents
    
    ## The integer amount of money paid
    paid = 0
    
    # Get an integer amount of the money paid
    for i in range(8):
        paid = payment[i]*Denominations[i] + paid 
    
    # Check the integer value paid
    print('Integer value paid: ', paid)
    
    ## An integer representing the change due in cents
    change = paid - price
    print('Your change is: ', change)
    
    ## A list represeting the change  
    chng = [0, 0, 0, 0, 0, 0, 0, 0]
    
    # Check the value of the change and return the appropriate amount
    if change < 0:
        # Insufficient payment
        print('Insufficient funds!')
        return None
    elif change > 0:
        # Calculate Change, and return array of correct change
        for j in range(8):
            num_denom = math.floor(change/Denominations[7-j])
            change = change - num_denom*Denominations[7-j]
            if num_denom > 0:
                chng[7-j] = num_denom
            else:
                chng[7-j] = 0
        print('Your change in number of denominations: ', chng)
        return chng
    else:
        # Exact change, return an array of zeros
        for k in range(0,7):
            chng[k] = 0
        return chng

# Test code to check the efficacy of the function
if __name__ == '__main__':
    price = "kek"
    payment = [-1, 0, 0, 0, 0, 0, 0, 0]
    change = getChange(price, payment)