'''
@file Lab3main.py
@brief A file to be used for collecting ADC voltage data on the Nucleo.
@details    This sets up an analog pin on the Nucleo board (Pin A0) that has 
            been shorted to the USER button pin (Pin C13) to record analog 
            data input. A timer is set up to control the data sampling rate, 
            and the sample array is 500 data points to allow for adequate time 
            to capture the step response from the capacitor connected to the
            USER button. First, the file waits for the proper input ('G' on 
            the front end), and then continuously fills the data buffer array
            until a satisfactory voltage difference between the first and last
            elements of the array has been met. Then, the data is put into a 
            comma-delineated format and sent to the frontend via UART.
@author Jacob Burghgraef
@date May 5th 2021

Documentation can be found here:
    https://bitbucket.org/jburghgraef/me405_labs/src/master/Lab%203/Lab3main.py
'''
from array import array
from pyb import UART, Pin, ADC, Timer, repl_uart

## Make sure debugging statements in PuTTY aren't sent over UART
# Also allows PuTTY to be open at the same time as Spyder
repl_uart(None)

## Create the Pin object connected to the blue user button
PA0 = Pin(Pin.cpu.A0, Pin.IN)

## Create the timer object to handle the timing of data sampling
timer = Timer(6, freq=50000)

## The UART serial communication port
ser = UART(2)

## The ADC object associated with Pin A0
adc = ADC(PA0)

## The memory buffer array, use this to take 5000 data points
buff = array('H', (0 for index in range(500)))

## The timestamp corresponding to the data in the buff array
time = 0

## Base time unit for each data point, in milliseconds
period = 0.02

print('Starting main.py')
    
def collect():
    '''
    @brief Continuously collects data until the step response has been recorded.
    '''
    while True:
        # Collect voltage data from analog pin A0
        adc.read_timed(buff,timer)
        # Check if the data has been collected by measuring a significant change in voltage
        if (buff[-1]-buff[0] > 3800):
            print('Data Collected, sending data to the frontend...')
            break;
        else:
            # User has not pressed button
            pass

while True:
    if (ser.any() != 0):
        # Check if the right command has been sent yet
        cmd = ser.readchar()
        if cmd == 71:
            print('User pressed G, beginning data Collection')
            collect() # Collect adc data
            time = 0 # Reset timestamp
            for data in buff:                
                # Write collected data to the serial port
                ser.write('{:}, {:}\r\n'.format(data,time))
                # Update Timestamp
                time += period
            print('Data sent to frontend.')
        else:
            # Wrong input
            pass