'''
@file Frontend.py
@brief A frontend file that handle data processing from the Nucleo.
@author Jacob Burghgraef
@date May 5th, 2021

Documentation for this file may be found here:
    https://bitbucket.org/jburghgraef/me405_labs/src/master/Lab%203/Frontend.py
'''
import serial
from array import array
import pandas as pd
import matplotlib.pyplot as plt

## The serial port object used to communicate with the nucleo
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

## The timestamp data array corresponding to collected ADC values
timestamp = array('f')

## The voltage data array
voltage = array('f') 

## The array of data from the collection task to be sent to a csv file
csv = []

def sendChar():
        '''
        @brief Sends an ascii character to the Nucleo.
        '''
        inv = input('Collect Data? (G == Start Collection): ')
        ser.write(str(inv).encode('ascii'))

def SaveDatainCSV():
        '''
        @brief Saves the data as a csv.
        '''
        df = pd.DataFrame(csv)
        df.to_csv('ADCvalues.csv', index=False)
        
def PlotData():
        '''
        @brief Plots the data using matlibplot.
        '''
        plt.plot(timestamp, voltage)
        plt.title('Voltage vs. Time')
        plt.xlabel('Timestamp (in millieconds)')
        plt.xlim([3,8]) # Adjust for time delay in button press signal
        plt.ylabel('Voltage (in Volts)')
        plt.show()

def CloseSerialPort():
        '''
        @brief Closes the Serial Port.
        '''    
        ser.close()
        
if __name__ == "__main__":

    # Query User for an input
    sendChar()
    
    # Wait until data is sent back from UART
    while True:
       if (ser.in_waiting != 0):
           break
    
    # Unpack the data sent from UART and fill the data and csv arrays
    with ser:
        for line in ser:
            [v,t] = line.decode('ascii').strip().split(',')
            timestamp.append(float(t))
            voltage.append(float(v)*3.3/4095)
            csv.append([t,v])
        
    PlotData()

    SaveDatainCSV()
    
    CloseSerialPort()