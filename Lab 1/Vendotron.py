'''
@file       Vendotron.py
@brief      Vendotron is a virtual vending machine.
@details    Vendotron is a virtual vending machine designed using a finite
            state machine with an inegrated generator; more details may be
            found in the VendotronFSM function. This file contains the 
            implemented FSM as well as a host of support functions that manage
            keylogging and calculating change.
@author     Jacob Burghgraef
@date       April 17, 2021

The image below shows a finite state machine describing Vendotron.
@image html "Vendotron FSM.jpg"

Documentation for this file can be found at: https://bitbucket.org/jburghgraef/me405_labs/src/master/Lab%201/Vendotron.py
'''

from time import sleep
import keyboard
import math

# The placeholder variable representing the most recently pressed key
last_key = ''

def kb_cb(key):
    '''
    @brief  The callback function called when a key is pressed.
    @param key The key that was just pressed.
    '''
    global last_key
    last_key = key.name
    
def resetkey(key):
    '''
    @brief  Resets the last_key variable.
    @param key The key that was just pressed.
    '''
    global last_key
    last_key = ''

# Tells the keyboard module to only respond to these specific keys
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)
keyboard.on_release_key("C", callback=kb_cb)
keyboard.on_release_key("P", callback=kb_cb)
keyboard.on_release_key("S", callback=kb_cb)
keyboard.on_release_key("D", callback=kb_cb)
keyboard.on_release_key("E", callback=kb_cb)
keyboard.on_release_key("B", callback=kb_cb)

def getChange(price, payment):
    '''
    @brief  Calculates the change from a given price and payment.
    @param price    The integer price of some given product in US cents.
    @param payment  The amount paid in US cents.
    @return chng    The amount of change returned; formatted in a list 
                    displaying the number of denominations of currency in 
                    each element of that list.
    '''
    
    # Check for integer parameters
    try:
    
    # Check if the price input is valid
        if int(price) < 0:
            invalid = "Invalid input, please put a positive integer for the price"
            print(invalid)
            return False
    
    # Check is the payment input is valid
        if payment < 0:
            invalid = "Invalid input, please put zero or a positive integer for the payment"
            print(invalid)
            return False
    
    # Catch improper data types in the parameter
    except ValueError:
        print("Invalid parameter, please input a positive integer for price.")
        print("For payment, input a positive integer or zero.")
        return False
    
    ## A list indexing the value of each acceptable denomination of US currency, all in cents
    Denominations = [
        1,              # Represents the value of a penny in cents
        5,              # Represents the value of a nickel in cents
        10,             # Represents the value of a dime in cents
        25,             # Represents the value of a quarter in cents
        100,            # Represents the value of a 1 dollar in cents
        500,            # Represents the value of a 5 dollars in cents
        1000,           # Represents the value of a 10 dollars in cents
        2000]           # Represents the value of a 20 dollars in cents
    
    ## The integer amount of money paid
    paid = payment
    
    ## An integer representing the change due in cents
    change = paid - price
    
    ## A list represeting the change  
    chng = [0, 0, 0, 0, 0, 0, 0, 0]
    
    # Check the value of the change and return the appropriate amount
    if change < 0:
        # Insufficient payment
        return None
    elif change > 0:
        # Calculate Change, and return array of correct change
        for j in range(8):
            num_denom = math.floor(change/Denominations[7-j])
            change = change - num_denom*Denominations[7-j]
            if num_denom > 0:
                chng[7-j] = num_denom
            else:
                chng[7-j] = 0
        return chng
    else:
        # Exact change, return an array of zeros
        for k in range(0,7):
            chng[k] = 0
        return chng

def VendotronTask():
    '''
    @brief      Runs one iteration of the Vendotron task
    @details    Runs throught the Finite state machine using a generator.
                The states contained include an initialization state (state 0)
                that displays the instructions for controlling Vendotron, an 
                idle state (state 1) that acts as a default state for detecting
                input from the user and directing the user to other states when
                necessary, it also keeps track of the balance and handles coin
                insertion. The second state displays a promotional message 
                after two minutes of inactivity. The third state handles the 
                dispensing of selected drinks (assuming the balance is sufficient)
                and it prompts the user to buy another drink if there is a 
                remaining balance. The fourth state displays an insufficient 
                funds message if the balance is less than the price of the 
                selected drink. The fifth state handles ejecting the balance
                and returning it to the user.1
    '''
    ## The state of the Vendotron FSM
    state = 0
    
    ## The current balance, representing the amount of money (in cents) remaining in Vendotron
    balance = 0
    
    ## A boolean representing whether or not a coin or bill has been inserted
    inserted = False
    
    ## The change to be dispensed to the user
    change = [0]*8
    
    ## A timer that controls when the promotional message is displayed
    timer = 0
    
    ## The price of a selected beverage, in cents
    price = 0
    
    ## The price of Cuke, in cents
    priceCuke = 129
    
    ## The price of Popsi, in cents
    pricePopsi = 107
    
    ## The price of Spryte, in cents
    priceSpryte = 150
    
    ## The price of Dr. Pupper, in cents
    priceDrPupper = 95
    
    ## A boolean representing whether a beverage has been selected or not
    bevSelect = False
    
    ## A boolean representing whether or not change has been calculated
    gotChange = False
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        
        
        if state == 0:
            # Initialization State
            print('Vendotron Initializing...')
            print('Welcome, and thank you for choosing the Vendotron!')
            print('To insert currency, press the following buttons on your keyboard at any time:')
            print('0 - inserts 1 US penny, worth 1 cent')
            print('1 - inserts 1 US nickel, worth 5 cents')
            print('2 - inserts 1 US dime, worth 10 cents')
            print('3 - inserts 1 quarter, worth 25 cents')
            print('4 - inserts 1 dollar, worth 100 cents')
            print('5 - inserts 5 dollars, worth 500 cents')
            print('6 - inserts 10 dollars, worth 1000 cents')
            print('7 - inserts 20 dollars, worth 2000 cents')
            print('To select a beverage, press the following buttons on your keyboard at any time:')
            print('C - selects Cuke, which costs $1.29')
            print('P - selects Popsi, which costs $1.07')
            print('S - selects Spryte, which costs $1.50')
            print('D - selects Dr. Pupper, which costs $0.95')
            print('To eject your change and deselect a beverage option, press E at any time.')
            print('To view your balance, press B at any time.')
            
            state = 1       # on the next iteration, the FSM will run state 1
            
        elif state == 1:
            # Idle state
            if last_key == '0':
                # Insert a penny
                print('Inserted 1 penny')
                # Update internal balance
                balance += 1
                # Set insert condition to true to display updated balance
                inserted = True
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == '1':
                print('Inserted 1 nickel')
                # Update internal balance
                balance += 5
                # Set insert condition to true to display updated balance
                inserted = True
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == '2':
                print('Inserted 1 dime')
                # Update internal balance
                balance += 10
                # Set insert condition to true to display updated balance
                inserted = True
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == '3':
                print('Inserted 1 quarter')
                # Update internal balance
                balance += 25
                # Set insert condition to true to display updated balance
                inserted = True
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == '4':
                print('Inserted 1 dollar')
                # Update internal balance
                balance += 100
                # Set insert condition to true to display updated balance
                inserted = True
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == '5':
                print('Inserted 5 dollars')
                # Update internal balance
                balance += 500
                # Set insert condition to true to display updated balance
                inserted = True
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == '6':
                print('Inserted 10 dollars')
                # Update internal balance
                balance += 1000
                # Set insert condition to true to display updated balance
                inserted = True
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == '7':
                print('Inserted 20 dollars')
                # Update internal balance
                balance += 2000
                # Set insert condition to true to display updated balance
                inserted = True
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == 'C':
                print('You have selected a Cuke!')
                # Set beverage selected trigger to true
                bevSelect = True
                # Set price to the price of Cuke
                price = priceCuke
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == 'P':
                print('You have selected a Popsi!')
                # Set beverage selected trigger to true
                bevSelect = True
                # Set price to the price of Popsi
                price = pricePopsi
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == 'S':
                print('You have selected a Spryte!')
                # Set beverage selected trigger to true
                bevSelect = True
                # Set price to the price of Spryte
                price = priceSpryte
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == 'D':
                print('You have selected a Dr. Pupper!')
                # Set beverage selected trigger to true
                bevSelect = True
                # Set price to the price of Dr. Pupper
                price = priceDrPupper
                # Reset pressed key
                resetkey(last_key)
                
            elif last_key == 'E':
                print('Ejecting balance, standby...')
                # Reset pressed key
                resetkey(last_key)
                # Go to Eject state
                state = 5
            elif last_key == 'B':
                print('Your balance is: ${:2.2f}'.format(balance/100))
                # Reset pressed key
                resetkey(last_key)
            
            # increment the timer up by 1
            timer += 1
            
            if (timer >= 7500):
                # Timer has reached 2 minutes, display the promotional message
                state = 2
            
            if (inserted):
                # Money was just inserted, update the balance
                print('Your balance is: ${:2.2f}'.format(balance/100))
                # Set inserted trigger to false
                inserted = False
            
            if (bevSelect and balance >= price):
                # Go to vend beverage state
                state = 3
            
            if (bevSelect and balance < price):
                # Go to insufficient funds state
                state = 4
            
        elif state == 2:
            # Promotion state
            
            # Promotional message
            print('Try cuke today!')
            print('Or maybe an ice-cold Popsi?')
            print('Spryte is all-new and all-tasty!')
            print('Having trouble deciding? Dr. Pupper is never a bad idea!')
            
            # Reset the timer
            timer = 0
            
            # Go back to the idle state
            state = 1
            
        elif state == 3:
            # Vend Beverage state
            
            # Get change in denominations of bills
            change = getChange(price, balance)
            
            # Reset to get new balance
            balance = 0
            # calculate new balance
            balance  = change[0]*1+change[1]*5+change[2]*10+change[3]*25+change[4]*100+change[5]*500++change[6]*1000+change[7]*2000
            
            # Deselct beverage
            bevSelect = False
            
            # Calculated change
            gotChange = True
            
            # Vend Beverage
            print('Vending selected beverage... ker-chunk! Please take your beverage.')
            
            if (balance == 0):
                # Empty Balance, thank customer and go back to idle state
                print('Thank you for choosing Vendotron for your beverage needs.')
                print('Please come again!')
                # Go back to idle state
                state = 1
                timer = 0 # Reset Timer
            elif (balance > 0):
                # Some balance is left, ask user if they want to buy a drink
                print('Your remaining balance is: ${:2.2f}'.format(balance/100))
                print('Please purchase another drink.')
                print('If you do not wish to purchase another drink, then press E to eject your funds.')
                # Go back to idle state
                state = 1
                timer = 0 # Reset Timer
            else:
                # Balance is negative, this shouldn't happen
                print('Error! Negative balance; Vendotron requires service to continue!')
                break
                
        elif state == 4:
            # Insufficient Funds state
            
            # Display insufficient funds message
            print('Insufficient Funds!')
            print('That beverage costs ${:2.2f}'.format(price/100))
            print('Your balance is ${:2.2f}'.format(balance/100))
            print('To buy that drink, you need ${:2.2f}'.format((price-balance)/100))
            print('Your beverage has been deselected; please insert more money and then reselect the beverage.')
            
            # Set beverage select to false
            bevSelect = False
            
            # Return to the Idle state
            state = 1
            timer = 0 # Reset timer
            
        elif state == 5:
            # Eject state
            
            if gotChange is False:
                # Get balance in denominations of US currency
                change = getChange(0, balance)
            else:
                # Change was already calculated
                pass
            
            print('Dispensing ${:2.2f}'.format(balance/100))
            print('Your change is:')
            print(str(change[0]), ' pennies')
            print(str(change[1]), ' nickels')
            print(str(change[2]), ' dimes')
            print(str(change[3]), ' quarters')
            print(str(change[4]), ' 1 dollar bills')
            print(str(change[5]), ' 5 dollar bills')
            print(str(change[6]), ' 10 dollar bills')
            print(str(change[7]), ' 20 dollar bills')
            print('Please take your change; thank you.')
            
            # Reset gotChange trigger
            gotChange = False
            
            # Reset Balance
            balance = 0
            
            # Return to state 1
            state = 1
            timer = 0 # Reset timer
            
        else:
            # this state shouldn't exist!
            
            pass
        
        yield(state)

if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')