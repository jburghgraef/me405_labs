'''
@file Lab0x02_Part_A.py
@brief This method uses external interrupts and utime to record reaction times.
@author Jacob Burghgraef
@date May 2nd, 2021

This file records reaction time using the USER button on the Nucleo and the 
built-in LED. It uses an external interrupt when the USER button is pressed 
(initialized on Pin C13) to callback to an ISR that records the time of the 
button press. All timing is handled by the utime function, and as such, the 
base time unit is 1 microsecond. The LED is controlled by a separate function
that takes the blink duration and start time as parameters, and controls the 
LED accordingly. The reaction time is calculated by finding the difference
in ticks between the start time of the LED and the timestamp at which the 
button is pressed.

The documentation for the source code can be found here:
    https://bitbucket.org/jburghgraef/me405_labs/src/master/Lab%202/Lab0x02_Part_A.py
'''

import utime
import pyb
from array import array
import random

# Create pin variables to interact with the Nucleo
## Pin variable representing Pin A5 (the LED) on the Nucleo
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## Pin variable representing Pin PC13 (the User button) on the Nucleo
pinC13 = pyb.Pin(pyb.Pin.cpu.C13, pyb.Pin.IN)

# Create lists and variables to catalogue reaction times
## A array of the recorded reaction times
rxn_times = array('i')

## A single recorded reaction time, used to add values to the reaction time array
rxn_time = 0

## Average reaction time
avg_rxn = 0

## A counter for the number of times the reaction time is measured
n = 0

## The time when the User button is pressed.
pressed_time = 0

## Duration of the LED blink, in microseconds
LED_dur = 10**6

## A boolean represent whether or not the User button has been pressed.
button_press = False

def pressed_isr(pin):
    '''
    Creates an Interrupt Service Routine (ISR) to indicate when a button has 
    been pressed.
    '''
    global pressed_time
    global button_press
    pressed_time = utime.ticks_us()
    button_press = True
    
# Generate an external interupt condition on Pin C13
extint = pyb.ExtInt(pinC13, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, pressed_isr)

def genWaitTime():
    '''
    Generates a random wait time between 2 and 3 seconds, in microseconds.
    '''
    ## A randomly selected time between 0 and 1 s, in microseconds.
    random_time = random.randint(0, 1000000)
    
    ## The time the program waits before turning the LED on
    wait_time = 2000000 + random_time
    
    return wait_time

def blinkLED(blink_start, blink_duration):
    '''
    Flashes the LED on the Nucleo for some amount of time.
    @param blink_start  The time at which the blinking starts, in microseconds.
    @param blink_duration   The amount of time the LED is on, in microseconds.
    '''
    pinA5.high()
    while True:
        # Check to see if the button has been pressed
        if (button_press == True):
            pinA5.low() # Turn off the LED
            break;
        else:
            # Check to see if the desired amount of time has passed
            if (utime.ticks_diff(utime.ticks_us(), blink_start) >= blink_duration):
                pinA5.low() # Turn off the LED
                break;
            else:
                pass

try:
    while True:
        # Run Reaction Time Test
        
        # Get wait time
        wait = genWaitTime()
        
        # Wait however many seconds before turning on the LED
        utime.sleep_us(wait)

        # Update the current timestamp
        start_time = utime.ticks_us()
        
        # Flash LED for 1 second, or until the button is pressed
        blinkLED(start_time, LED_dur)
        
        # Calculate the reaction time
        rxn_time = utime.ticks_diff(pressed_time, start_time)
        
        if (button_press == False):
            print('Something went wrong, you did not press the button.')
            print('Please re-run the program and be sure to press the blue button as soon as the green LED flashes.')
            break;
        
        # Append the reaction time value to the reaction time array
        rxn_times.append(rxn_time)
        
        # Increment counter
        n += 1
        
        # Display the reaction time for this run
        print('Reaction ', str(n), ' in microseconds: ', str(rxn_time))
        
        # Reset Pressed Timestamp
        pressed_time = 0
        
        button_press = False
        
except KeyboardInterrupt:
    print('Ctrl+C detected, displaying results.')
    print('Number of reaction tests run: ', str(n))
    
    # A placeholder for the sum of the reaction times, in microseconds
    rxn_sum = 0
    for i in range(len(rxn_times)):
        rxn_sum += rxn_times[i]       # Calculate the sum of the reaction times
    
    # Calculate the average reaction time, in microseconds
    avg_rxn = rxn_sum/n
    
    print('Average reaction time in microseconds: ', str(avg_rxn))
    print('Average reaction time in milliseconds: {:3.1f}'.format(avg_rxn/10**3))
    print('Average reaction time in seconds: {:1.4f}'.format(avg_rxn/10**6))