'''
@file TPShares.py
@brief      A shared variables and queues file.
@details    This file acts as an intermediary that holds all the shared 
            variables between each task in the balancing platform system. Each 
            shared variable is a Share object from the task_share file and each
            queue is a Queue object from the task_share file.
@author Jacob Burghgraef & Alyssa Liu
@date June 11th, 2021

The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/TPShares.py
'''

from task_share import Share, Queue


## The share object used to transfer the "all-clear" command
allclr = Share('I', thread_protect = True, name = "allclr")
         
## The share object used to signal the fault flag
faultflag = Share('I', thread_protect = True, name = "faultflag")
        
## The share object used to signal the halt command
halt = Share('I', thread_protect = True, name = "halt")
        
## The share object used to signal the begin balancing command
begbal = Share('I', thread_protect = True, name = "beginbal")

## The shares object representing the DTY cycle for the X Motor
dtyX = Share('f', thread_protect = True, name = "dtyX")
         
## The shares object representing the DTY cycle for the Y Motor
dtyY = Share('f', thread_protect = True, name = "dtyY")

# The following Shares are used specifically for closed loop feedback

## The Share object representing the measure x positional data
x = Share('f', thread_protect = True, name = "x")

## The Share object representing the measure y positional data
y = Share('f', thread_protect = True, name = "y")

## The Share object representing the measure xdot velocity data
xdot = Share('f', thread_protect = True, name = "xdot")

## The Share object representing the measure ydot velocity data
ydot = Share('f', thread_protect = True, name = "ydot")

## The Share object representing the measure theta x positional data
th_x = Share('f', thread_protect = True, name = "th_x")

## The Share object representing the measure theta y positional data
th_y = Share('f', thread_protect = True, name = "th_y")

## The Share object representing the measure theta xdot velocity data
th_xdot = Share('f', thread_protect = True, name = "th_xdot")

## The Share object representing the measure theta ydot velocity data
th_ydot = Share('f', thread_protect = True, name = "th_ydot")

# The following queues are used specifically for data collection

## The Queue object representing the measure x positional data
xdc = Queue('f', 200, thread_protect = True, overwrite = True, name = "xdc")

## The Queue object representing the measure y positional data
ydc = Queue('f', 200, thread_protect = True, overwrite = True, name = "ydc")

## The Queue object representing the measure xdot velocity data
xdotdc = Queue('f', 200, thread_protect = True, overwrite = True, name = "xdotdc")

## The Queue object representing the measure ydot velocity data
ydotdc = Queue('f', 200, thread_protect = True, overwrite = True, name = "ydotdc")

## The Queue object representing the measure theta x positional data
th_xdc = Queue('f', 200, thread_protect = True, overwrite = True, name = "th_xdc")

## The Queue object representing the measure theta y positional data
th_ydc = Queue('f', 200, thread_protect = True, overwrite = True, name = "th_ydc")

## The Queue object representing the measure theta xdot velocity data
th_xdotdc = Queue('f', 200, thread_protect = True, overwrite = True, name = "th_xdotdc")

## The Queue object representing the measure theta ydot velocity data
th_ydotdc = Queue('f', 200, thread_protect = True, overwrite = True, name = "th_ydotdc")