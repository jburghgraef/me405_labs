'''
@file BNO055.py
'''

#from pyb import I2C

class BNO055:
    '''
    An object representing the BNO055 IMU, it has 3 functions:
        1. modus_operandi(), which changes the operation mode of the IMU
        2. calibrate(), retrieves the calibration status from the IMU
        3. Euler(), reads Euler angles from the IMU to use as state measurements
        4. AngVel(), reads angular velocity from the IMU to use as state measurements
    '''
    def __init__(self, I2C, address):
        '''
        Creates a BNO055 object.
        @param  I2C     The I2C object that links the MCU to the IMU, should 
                        be passed in preconfigured in master mode.
        @param  address The address to the IMU register that contains the 
                        desired data.
        '''
        
        ## The I2C object that interfaces with the sensor
        self.I2C = I2C
        
        ## The address to the register that contains the data of interest
        self.address = address
        
        ## An array to hold the data read from the address
        self.data = bytearray(6)
        
        # I'm not sure how I'm supposed to read from these functions...
        # If you want to pick up where I left off, I was trying to decipher 
        # this github code: https://github.com/micropython-IMU/micropython-bno055/blob/master/bno055_base.py