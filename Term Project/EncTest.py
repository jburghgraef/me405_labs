'''
A test file for the encoders
'''

from EncoderDriver import EncoderDriver
from pyb import Pin, Timer
import utime

## Pin 1 of Encoder 1
PB6 = Pin(Pin.cpu.B6)
## Pin 2 of Encoder 1
PB7 = Pin(Pin.cpu.B7)
## The timer for Encoder 1
tim1 = Timer(4, prescaler = 0, period = 0xFFFF)
## Encoder 1, directly measuring rotation about the Motor 1's Y axis,
#  and indirectly measuring rotation about the platform's X axis
Enc1 = EncoderDriver(PB6, PB7, tim1, 40000)

while True:
    utime.sleep(1)
    Enc1.update()