"""
@file Results.py

@section sec_titleSnD Results and Discussion

@author Jacob Burghgraef and Alyssa Liu

@date June 11th, 2021

@section sec_overviewResults Overview of Results

Our results are rather disappointing yet still encouraging, for we got the 
balancing platform to respond to the ball’s movement. However, the platform 
does not properly balance the ball - in almost all test cases, the ball would 
either be flung off the platform due to an overly aggressive response or it 
would roll off the platform due to a fault occurring during the response. In 
fact, a fault occurred in each test where the platform responded to the ball.

@section sec_smpleotpt Sample Output from the User Interface

@image html "UI Output.JPG" width=75%

Note: For some reason, the program would often enter a fault state upon 
startup. This can be easily circumvented by pressing the all clear key to 
return to the idle state. Additionally, the data collection task wasn’t 
writing data to the csv because the system would reach a fault state before 
the data collection task was able to record data. Thus, to plot the faulty 
responses, we printed the recorded data to the Putty REPL after the fault was 
detected. 

@section sec_spreads Spreadsheet and Plots

Using the data printed to the Putty REPL, we tabulated responses with 8 or more 
data points in a spreadsheet, which is shown below. Timestamps were 
approximated with a time step equal to the period of the touch panel and 
encoder tasks because not all the data was collected at the same time. 
Furthermore, collecting the timestamps of each data point would be difficult 
to accurately estimate and it would lead to confusing data presentation.

@image html "Spreadsheet Analysis.JPG" width=50%

The plots shown below compare the responses of two similar tests with the same 
number of data points. The first plot compares the path of the ball between 
the two tests in the plane of the touch panel as time passes. The second and 
third plots compare how the velocity responses in the X and Y directions vary 
with time for each test case. For each plot, the blue circles represent data 
from test 1 and the orange triangles represent data from test 2.

@image html "Results Plot 1.png"

This one is the most meaningful, for it shows the path the ball takes during 
the balancing routine. The axes have been adjusted to approximate the 
dimensions of the touch panel, and the timestamps for each data point have 
been added to show the general path the ball took as time went on. The cluster 
of points at the beginning of each response suggest the ball rolls around a 
bit as the system moves slightly to adjust the position of the ball. Then, the 
response of the system diverges as time passes, with a fault likely occurring 
around 80 ms for test 1 and around 160 ms for test 2. The responses both seem 
to travel in the correct direction along the x axis, but they fall off along 
the y axis. This suggests an error in either the gain values for the y 
response (these were assumed to be the same as the gain values for the x axis), 
in the measured data for calculating the y axis response, or an error in the 
calculation of the y axis response.

@image html "Results X Velocity.png"
@image html "Results Y Velocity.png"

The velocities are approximated by using the change in position in between 
touch panel task runs divided by the period between touch panel task runs. The 
velocity responses do appear to follow the expected response given the path of 
the ball shown in the first plot. That is, they increase slightly in the 
positive direction and then diverge to the negative direction as the 
controller attempts to overcompensate for the increasing deviation from the 
mean. This response makes the most sense for the Y axis since it’s generally 
greater in magnitude than the response in the X axis, which occurs due to the 
ball falling off in the Y direction. The response in the X direction should be 
positive, but it’s likely that due to the fault occurring early in the 
response, the controller was unable to properly compensate.

@section sec_video Video Demo Link

\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/pQ4qMuhTgJc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly

Here's the link if the video doesn't work: https://youtu.be/pQ4qMuhTgJc

The link above shows our several attempts at balancing the ball, but to little 
success. There was one attempt that got pretty close at balancing the ball, 
but the ball didn’t quite roll back to the center, so that attempt can’t quite 
be considered as a successful run. Additionally, at the time of recording the 
videos, data collection wasn’t working properly, so the data plots above don’t 
reflect any of the attempts shown in the video. From the various trials in the 
video, it’s quite clear that the success of the response not only relies on 
the aggressiveness controller gain values, but also the initial conditions. If 
the ball is placed farther away from the center of the touch panel, then the 
response is generally more aggressive, and if it’s closer, the response is 
somewhat milder, although all responses would end in divergence.

@section sec_why Why didn't the system work?

The reason why our balancing platform didn’t perform as expected is primarily 
due to the lack of testing time. If we had a few more days of testing time, we 
would have been able to tune our controller and resolve more bugs in the code. 
Additionally, we might have been able to implement the alpha-beta filter for 
the touch panel and encoder data, fix the data collection task, and optimize 
the frequency at which each task ran in our scheduler. Implementing an 
Inertial Measurement Unit (IMU) would have also helped by automatically 
pre-balancing the balancing platform and providing some error checking for 
encoder measurements. There were many challenges with the hardware, so it’s 
not unreasonable to assume that there are still unknown hardware issues 
contributing to the poor performance. The model we used to develop the 
controller gains oversimplified much of the system, which means the gain 
values are likely of little value much beyond signalling the relative 
magnitudes between each gain value. During testing, the gain values were 
varied within 1 order of magnitude of the derived gain values in addition to 
some variations with the signs of the gains in the torque calculations. Thus, 
it’s possible that the responses got worse during testing as the gain values 
deviated from their original values. Typically, the gain values kept their 
approximate relative magnitude while their absolute magnitude was changed in 
the direction that appeared to better the system response.
"""